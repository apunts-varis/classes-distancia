# Recursos XTEC (Xarxa Telemàtica d'Educació de Catalunya)

## Credencials dels professors (Educativa i d'empleat)

Els professors tenim un usuari XTEC per a temes educatius.

**Usuari XTEC** (Correu, Certificats). Ex: jgarcia25@xtec.cat - password del compte de correu XTEC.
- **Alta de compte XTEC** - http://xtec.gencat.cat/ca/at_usuari/gestio/altausuari/
- **Atenció a l'usuari** - Canvi de contrasenya, etc. - http://xtec.gencat.cat/ca/at_usuari/
- **Gestió de correu** - http://gmail.com i escriure el correu xtec al nom d'usuari (ex: jgarcia25@xtec.cat). El Dept. d'Ensenyament es va "aliar" amb Google...
  - **ATENCIÓ**: No canvieu la contrasenya des de l'aplicatiu de correu de Google per que llavors no tindreu els comptes XTEC i correu sincronitzats (tindran contrasenyes diferents).
- **Eduroam** - Wifi gratuïta per a formadors arreu d'Europa - http://www.xtec.cat/eduroam
- **Consulta certificats i formació** - https://aplicacions.ensenyament.gencat.cat/pls/e13_cert_pub/certificacio_formacio.menu_opcions

Tenim també una credencial corporativa per a tasques administratives i de gestió del nostre perfil laboral.

**Usuari ATRI** - Credencials corporatives (SAGA, ATRI, Intranet). Ex: NIF + password o bé certificat digital (de la FNMT o IdCAT, no caldrà usuari ni password).
- Gestió de contrasenyes (recuperar) - https://idp4-gicar.gencat.cat/autogestio/iniArhpa.jsp

L'usuari ATRI es fa servir per el nostre ordinador de professor, al menys la contrasenya. El nom d'usuari és el del compte XTEC seguit de @edu.gencat.cat. Ex: pep22@edu.gencat.cat

---

## Curs d'autoformació serveis XTEC

- La meva XTEC - El compte XTEC ofereix una sèrie de serveis telemàtics i incorpora elements i funcionalitats avançades de col·laboració i comunicació - http://xtec.gencat.cat/ca/at_usuari/lamevaxtec/

- Curs "La meva XTEC" - Recursos XTEC explicats - http://alexandria.xtec.cat/course/view.php?id=556

![](imgs/webs-xtec-83b674c1.png)

---

## WEBS Departament d'Ensenyament

- Dept d'educació a gencat.cat - http://ensenyament.gencat.cat

- Tria Educativa - https://triaeducativa.gencat.cat/ca/fp/cursos-especialitzacio/

- Xarxa Telemàtica Educativa de Catalunya - http://xtec.gencat.cat
  - Centres: http://xtec.gencat.cat/ca/centres/

- Projectes d'Ensenyament - https://projectes.xtec.cat
- Projecte d'acceleració digital - https://projectes.xtec.cat/edc/
  - Portàtils per a professors COVID - https://projectes.xtec.cat/edc/docents/
  - Portàtils per a alumnes COVID - https://projectes.xtec.cat/edc/alumnes/

---

## WEBS PER A PROFES (usuari XTEC)

**Formació**
- **Espai de formació XTEC** - http://xtec.gencat.cat/ca/formacio/
- **Ateneu** - espai que recull els materials elaborats per a les activitats formatives, recursos metodològics i documentals, eines per treballar a les aules i tutorials - http://ateneu.xtec.cat/
- **Odissea** - Plataforma d'autoformació professorat - https://odissea.xtec.cat
- **Cultura digital (competència digital)** - http://xtec.gencat.cat/ca/formacio/formaciogeneralprofessorat/cultura-digital/

**Recursos TIC**
- **Àgora** - Moodle i web (nodes) per a centres - https://agora.xtec.cat
- **Alexandria** - Repositori de recursos educatius - http://alexandria.xtec.cat
- **Blocs** - Plataforma de blocs - https://blocs.xtec.cat/
- **Edu365.cat** - Materials didàctics - http://www.edu365.cat
- **Edu3.cat** - Vídeos de TV3 per finalitats educatives - http://www.edu3.cat/
- **Educat2.0** - Xarxa d'acompanyament i formació TIC - https://educat.xtec.cat/
- **L’ARC** és un espai al servei dels mestres i del professorat per compartir propostes docents de qualitat associades a les competències bàsiques i al currículum - https://apliense.xtec.cat/arc/que-es-l-arc
- **Merlí** és un catàleg de recursos educatius digitals desenvolupat pel Departament d'Ensenyament - http://merli.xtec.cat
- **JCLic** - El Clic està format per un conjunt d'aplicacions de programari lliure que permeten crear diversos tipus d'activitats educatives multimèdia. - https://clic.xtec.cat
- **Linkat** - Distribució Linux educativa de la Gencat -  http://linkat.xtec.cat/

---

## WEBS Administratives (usuari ATRI)

- **ATRI** - Accés a dades personals i administratives - https://atriportal.gencat.cat/
- **Intranet Educació** - Accés a documentació de centres, etc. -  https://inici.espai.educacio.gencat.cat/
- **SAGA** - Gestió dels alumnes FP - https://saga.xtec.cat/
- **ESFERA** - Gestió dels alumnes secundària i primària - https://bfgh.aplicacions.ensenyament.gencat.cat/bfgh/#/

---

## FORMACIÓ

### XTEC

- Reconeixement d'activitats de formació permanent per al professorat: https://web.gencat.cat/ca/tramits/tramits-temes/reconeixement-formacio-professorat?moda=1
- Certificats reconeguts pel departament Educ: https://xtec.gencat.cat/ca/formacio/la-meva-formacio/els-meus-certificats/
- Podeu revisar sol·licituds, consultar assignacions i omplir qüestionaris mitjançant usuari XTEC o clau d'inscripció:
  - https://xtec.gencat.cat/ca/formacio/la-meva-formacio/les-meves-activitats/
  - https://aplicacions.ensenyament.gencat.cat/pls/soloas/pk_for_mod_ins.p_for_form_cons_selec

### Ministerio

- Cursos del Ministerio Educ. FP y deportes: https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/catalogo/profesorado/profesorado-no-universitario.html

- INTEF: Aprendizaje en línea para una transformación digital de la educación - https://aprende.intef.es/

- Seu electrònica Ministerio Educ. FP y deportes: https://sede.educacion.gob.es/sede/login/inicio.jjsp?iA=no

- Convocatorias de Educación próximas al cierre: https://www.educacionfpydeportes.gob.es/servicios-al-ciudadano/catalogo/convocatorias-al-cierre.html

### Privats i reconeguts pel departament

- Baremo de méritos para las oposiciones a los cuerpos de Técnicos Superiores en Educación Infantil, Maestros de las etapas de Ed. Infantil y Ed. Primaria, Profesores de Enseñanza Secundaria y de Formación Profesional, y Escuela Oficial de Idiomas con nuestros cursos 100% online de Formación Permanente del Profesorado. Impartidos y certificados por UNIR, la Universidad Internacional de La Rioja. Además, nuestros cursos también son baremables en concursos de traslados docentes y puntúan dentro del apartado de Formación y Perfeccionamiento. No son válidos para sexenios. - https://www.cenhomologados.com/

- Els cursos per a docents formen part del pla de formació permanent, reconeguts pel Departament d'Educació de la Generalitat de Catalunya. - https://aprenonline.org/

## TI

- Cisco: https://www.netacad.com/catalogs/learn

---

## ALTRES

- **Consell escolar de Catalunya** - http://consellescolarcat.gencat.cat
- **Consell Superior d'Avaluació del Sistema Educatiu** - http://csda.gencat.cat
- **Sinapsi** recull les notícies que publiquen els centres educatius catalans a les seves pàgines web. Fa visible la major part del treball desenvolupat pels centres i busca promoure sinergies entre ells. - https://sinapsi.xtec.cat/ca/

## PROGRAMES INNOVACIÓ

- Programa d’Innovació d’Acceleració de la Transformació Educativa / Eines per al Canvi - https://projectes.xtec.cat/transformacioenxarxa/programa-dinnovacio-dacceleracio-de-la-transformacio-educativa/

## Escriptoris virtuals

- IsardVDI - A partir del 20 de desembre de 2024, només podràs accedir a la plataforma amb el compte edu.gencat.cat - https://elmeuescriptori.gestioeducativa.gencat.cat/login/all
