## Alguns repositoris d'imatges:

- **Wikimedia Commons**, on totes tenen una llicència genèrica CC by-nc-sa o bé una altra de menys restrictiva.

- **Flickr** permet trobar, des de la cerca avançada, imatges que tinguin llicències Creative Commons. Flickr facilita la sol·licitud autorització per utilitzar imatges que tenen drets reservats (Gettyimages).

- **Patrimoni.gencat**, entre d'altres institucions catalanes, disposen de col·leccions de fotografies a Flickr, amb llicències Creative Commons.
- **Banc d'imatges de l'INTEF**, que en permet un ús educatiu no comercial.

- **Open Clip Art**, icones que pertanyen al domini públic i que, per tant, es poden utilitzar lliurement. La imatge de la dreta prové d'aquest repositori.

- **Google** també disposa d'un sistema de cerca avançada que permet restringir la cerca a documents, llocs web i imatges que hagin especificat llicències que en permetin fer-ne ús sense haver de sol·licitar el permís corresponent.

Recordeu que tot i que se'n permeti fer un ús lliure és necessari citar la font i el nom de l'autor/a de la imatge, fins i tot per fer-ne un ús educatiu.

Més informació: " Repositoris d'imatges per publicar digitalment" (Bloc de Gencat).

- **The noun project** - Icones de tot tipus - https://thenounproject.com/
