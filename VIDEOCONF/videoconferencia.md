# Video conferència

## Jitsi Meet:

- Permet iniciar videoconferència des del navegador sense tenir cap compte.

- Amb Docker: https://github.com/jitsi/docker-jitsi-meet

## Zoom
## Whatsapp
## Skype
## Google Meet

- Extensió per demanar torn de paraula - https://chrome.google.com/webstore/detail/nod-reactions-for-google/oikgofeboedgfkaacpfepbfmgdalabej/related

- Extensió per passar llista automàticament amb el meet - https://chrome.google.com/webstore/detail/meet-attendance/nenibigflkdikhamlnekfppbganmojlg?hl=ca

- Extensió cuadrícula - https://chrome.google.com/webstore/detail/google-meet-grid-view/kklailfgofogmmdlhgmjgenehkjoioip

- Emojis - https://chrome.google.com/webstore/detail/nod-reactions-for-google/oikgofeboedgfkaacpfepbfmgdalabej/related

- Efectes especials (blur, background blur, croma verd, etc) - https://chrome.google.com/webstore/detail/visual-effects-for-google/hodiladlefdpcbemnbbcpclbmknkiaem/related

- Botó ràpid posa treu micro - https://chrome.google.com/webstore/detail/google-meet-push-to-talk/pgpidfocdapogajplhjofamgeboonmmj

- Transcripcions a text - https://chrome.google.com/webstore/detail/tactiq-pins-for-google-me/fggkaccpbmombhnjkjokndojfgagejfb/related

- Pissarra - https://chrome.google.com/webstore/detail/google-meet-classroom-ext/kbdbkfhklpbhpigonbdbclmnkmogfecl

- Meet plus - Ajustes configurables,emojis,estado,pizarra,notas,quiz,citas inspiradoras,temporizador,silencio,enfoque para Google Meet - https://chrome.google.com/webstore/detail/google-meet-plus/lbfjgknkjfjmnjdgdhbbmmbkoddgpdoc

---

## Discord

### Anuncis
Bot sesh, calendari per Discord. https://sesh.fyi/manual/

### Attendance en Discord

- Cal instal·lar un bot anomenat Suivix. Obrim https://suivix.xyz/en
  ![](assets/videoconferencia-9f3d82d1.png)
  - Ens obrirà l'autenticació de Discord.
  ![](assets/videoconferencia-ca1500e5.png)
  - Li indicarem el servidor al que volem afegir el bot.  
  ![](assets/videoconferencia-fb99bcb0.png)
- Per crear un nou attendance, que no és més que una llista dels presents a un canal, cal:
  1. Escriure !suivix al chat de text on vulguem que aparegui l'attendance.
  ![](assets/videoconferencia-6cab2430.png)
  2. Picar l'enllaç que ens apareix.
  4. Ara escollim el rol dels usuaris i el canal on volem prendre l'assistència. Per exemple per a SMX2A en el canal de M04:
  ![](assets/videoconferencia-9909d233.png)

NOTA: Si el bot ja hi és, només cal fer !suivix per iniciar un nou attendance.

### Drets que demana al rol

- Els que no es veuen estan apagats
![](assets/videoconferencia-456616c5.png)
![](assets/videoconferencia-5a357f6f.png)
![](assets/videoconferencia-2344ec19.png)
