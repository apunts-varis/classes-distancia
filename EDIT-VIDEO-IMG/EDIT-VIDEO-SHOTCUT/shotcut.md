# Editor de vídeo Shotcut

## Tutoriales

- Tutorial (funcionamiento en general) Laura Folgado - https://www.youtube.com/watch?v=RXWVsj18_w0


## Pantalla inicial

La interfície té aquesta aparença:

![](imgs/shotcut-1e39167b.png)

Amb aquest botó podem commutar entre diferents eines, com ara la llista de reproducció i els filtres (si hem posat a visualització els dos ítems).

![](imgs/shotcut-1cc5b4b4.png)

També tenim les pestanyes a sota per canviar d'ítem a visualitzar

![](imgs/shotcut-8faf7b2e.png)

---

## Gestió del pojecte

https://forum.shotcut.org/t/project-management/12574

---

## Crear projecte nou

Els projectes en ShotCut són carpetes que contenen els arxius de definició del projecte. És recomanable posar els vídeos origen a la mateixa carpeta.

Crearem un projecte nou. Per afegir vídeos a la llista de reproducció piquem afegir i seleccionem més d'un si volem unir-los.

![](imgs/shotcut-a9a261ec.png)

![](imgs/shotcut-dc8a6161.png)

També podem obrir una finestra d'explorador d'arxius i arrossegar els vídeos directament sobre l'aplicació Shotcut (a la llista de reproducció o sobre la pantalla de visualització).

---

### Posar vídeos a la línia de temps

La part inferior de la interfície que apareix buida és la que conté les línies de temps. Per a posar vídeos a la línia de temps, de forma seqüencial, seleccionem els dos vídeos (CTRL+click) i els arrosseguem cap a la línia de temps (a la part inferior).

![](imgs/shotcut-cec05403.png)

La línia de temps ara té dos vídeos un darrera l'altre. L'espai verd que es veu davant és una zona sense vídeo (negre, sense so). Entre els dos vídeos també ho podem tenir. Arrossegant els clips de vídeo (trossos) podem disminuir o ampliar aquest espai sense vídeo.

![](imgs/shotcut-3475339b.png)

Podem veure les línies de temps més estretes si a la capçalera de línia de temps piquem botó dret i escollim del menú contextual "Fes les pistes més estretes" o CTRL+"-".

---

### Tallar vídeo i fer clips

Posem la barra de punt actual al lloc on volem tallar i piquem el botó "tallar". Això genera un "clip". Cada clip es pot seleccionar i te atributs propis.

![](imgs/shotcut-a924db74.png)

- Arrossegant els clips els podem moure de lloc.
- Amb el botó dret a sobre del clip podem fer operacions com ara esborrar per a treure trossos que no volem al resultat final.

---

### Transicions entre clips

- Si en arrossegar un clip, muntem un a sobre de l'altre, estem indicant que volem una transició entre els dos.

![](imgs/shotcut-6a6bbe9f.png)

- Amb botó dret a sobre la transició podem veure les seves propietats i escollir el tipus i la suavitat de la transició.

![](imgs/shotcut-24f9edd0.png)

- Desplegant el menú de Vídeo tenim totes les opcions de transició de vídeo. També podem gestionar la suavitat i la transició de l'àudio.

![](imgs/shotcut-e490fb0e.png)

---

### Afegir filtres

- Si volem per exemple pujar el volum d'un clip, el seleccionem i li podem posar un filtre. Cal picar la pestanya filtre i amb el botó + afegir el que necessitem.

![](imgs/shotcut-2d82866b.png)

![](imgs/shotcut-4925706a.png)

![](imgs/shotcut-1267fc55.png)

- Si volem aplicar el filtre a tot el vídeo, fem-ho abans de començar a tallar clips, per que si no haurem d'aplicar a cada clip per separat.

- D'altres filtres permeten modificar el clip.

---

### Afegir pistes d'audio o vídeo (o les dues)

- Amb botó dret a la capçalera de la pista, ens apareix el menú contextual de pistes. Podem afegir una pista de vídeo, audio o totes dues. La idea és que es superposen les pistes que s'executen de manera simultània. Així, per exemple podem posar textos a la pista de dalt que aniran apareixent en el moment que toqui.

![](imgs/shotcut-3ba509f7.png)

- La pista de dalt és una capa superior i tapa la de sota. Tot el que no estigui en transparent a la de dalt, taparà el que hi hagi a la de baix. Normalment apliquem filtres a la de dalt per que hi hagi parts transparents i es vegi la de baix, segons algun efecte (per exemple imatge sobre imatge o pantalla dividida).

---

### Afegir text al vídeo

- Podem posar el text en una línia de temps de vídeo addicional per a controlar quan apareix i desapareix el text. Se superposarà al vídeo principal.

- Piquem el botó "Obre Altres" (al costat d'obre fitxer) i escollim "Text". Escrivim el text i les propietats que volem modificar.

![](imgs/shotcut-f594c56d.png)

- Un cop ajustat, arrosseguem des de la finestra de visualització cap a la línia de temps de dalt.

![](imgs/shotcut-da495225.png)

![](imgs/shotcut-fafd10f3.png)

- El resultat és que es superposa el text sobre el vídeo durant el temps indicat a la línia de temps de dalt.

![](imgs/shotcut-936f7eb8.png)

---

### Fer fade-out o Fade-in

Agafar l'esquina superior dreta d'un clip i arrossegar el punt que apareix cap a l'equerra per a fer fade-out. S'indica amb una línia diagonal des del principi de l'efecte cap a l'esquina.

![](imgs/shotcut-dc0b9ec6.png)

![](imgs/shotcut-10348054.png)

Es pot fer un fade-in fent el mateix amb l'esquina superior esquerra del clip cap a la dreta.

---

### Efecte PiP (Picture in Picture) o pantalla dividida

1. Afegim el vídeo principal per crear la primera pista de vídeo.
2. Crearem una segona pista de vídeo a sobre i hi posarem el vídeo que volem veure en petit.
3. Seleccionem el vídeo "petit" i li posem el filtre "Size, position and Rotate", que ens permet canviar la mida, posició i rotació del vídeo.
4. Podem posar-ho en petit

![](imgs/shotcut-6e0d601b.png)  

5. Podem posar-ho de forma que hi hagi un efecte de pantalla dividida (aplicant el filtre a la segona línia de temps a sota). Si volem que la imatge ocupi tota la seva meitat, caldrà marcar "distorsió".

![](imgs/shotcut-0f0715dc.png)

---

### Exportar vídeo resultat

Un cop tenim els talls ordenats, podem generar el vídeo final.

- Anar a fitxer->exporta el vídeo

![](imgs/shotcut-e852561d.png)

- És important assegurar-se de no fer servir l'acceleració per hardware si no tenim targeta gràfica acceleradora. El vídeo sortirà amb audio només i vídeo en negre si no està ben configurat.
- Es pot configurar diferents opcions de format de vídeo i audio, picant Avançat.

![](imgs/shotcut-c17faf78.png)

- Podem veure el diàleg d'exportació picant el botó de la barra superior "Exporta".
- Ara cal picar la pestanya Exporta i començarà el procès. Si volem veure les tasques en marxa (de generació del vídeo podem tenir més d'un a la vegada), piquem el botó de la barra superior "Treballs".

![](imgs/shotcut-b61ac497.png)
