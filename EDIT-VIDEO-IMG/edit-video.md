# Recull d'eines d'edició i gravació de Vídeo

https://www.sololinux.es/los-mejores-editores-de-video-del-2019/

## Edició web

- https://mastershot.app/

---

## Captura de pantalla (Android)

adv screen recorder

## Captura de vídeo de pantalla (PC)

### VokoScreen

Molt bona utilitat per a gravar tot el que passa a pantalla i sobreimpressionar la teva webcam. A més marca si vols quines tecles pitjes i quan fas click amb el ratolí.
- https://linuxecke.volkoh.de/vokoscreen/vokoscreen.html

---

### Simple Screen Recorder

https://www.maartenbaert.be/simplescreenrecorder/

Features

  1. Graphical user interface (Qt-based).
  2. Faster than VLC and ffmpeg/avconv.
  3. Records the entire screen or part of it, or records OpenGL applications directly (similar to Fraps on Windows).
  4. Synchronizes audio and video properly (a common issue with VLC and ffmpeg/avconv).
  5. Reduces the video frame rate if your computer is too slow (rather than using up all your RAM like VLC does).
  6. Fully multithreaded: small delays in any of the components will never block the other components, resulting is smoother video and better performance on computers with multiple processors.
  7. Pause and resume recording at any time (either by clicking a button or by pressing a hotkey).
  8. Shows statistics during recording (file size, bit rate, total recording time, actual frame rate, ...).
  9. Can show a preview during recording, so you don't waste time recording something only to figure out afterwards that some setting was wrong.
  10. Uses libav/ffmpeg libraries for encoding, so it supports many different codecs and file formats (adding more is trivial).
  11. Can also do live streaming (experimental).
  12. Sensible default settings: no need to change anything if you don't want to.
  13. Tooltips for almost everything: no need to read the documentation to find out what something does.


  If you are using Ubuntu 17.04 or newer, SimpleScreenRecorder can be found in the official repositories. You can install it with:

  sudo apt-get update
  sudo apt-get install simplescreenrecorder
  Add this if you want to record 32-bit OpenGL applications on a 64-bit system:

  sudo apt-get install simplescreenrecorder-lib:i386
  You should copy-paste these commands to a terminal line by line rather than all at once, otherwise it won't work.

  If you are using an older version of Ubuntu, or you want to get the latest version of SimpleScreenRecorder immediately without waiting for the next Ubuntu release, you can also get the package(s) from the SimpleScreenRecorder PPA:
~~~
  sudo apt-add-repository ppa:maarten-baert/simplescreenrecorder
  sudo apt-get update
  sudo apt-get install simplescreenrecorder

//  Add this if you want to record 32-bit OpenGL applications on a 64-bit system:

  sudo apt-get install simplescreenrecorder-lib:i386
~~~

---

### http://www.moovly.com

---

### Loom

- Captura pantalla amb la inclusió de la teva imatge. Propietària i pagament

https://www.loom.com/

---

### Screencastify

- Extensió de Google Chrome. Permet capturar vídeo de pantalla i pestanya al navegador. També captura audio en mode pestanya.
- Límit de 5 minuts per vídeo, en versió gratis
- screencastify.com

---

### OBS Studio

- Es poden crear escenes amb tot preparat i visualitzar o no.
- Cada escena té diferents components i capes que es poden obtenir de fonts diferents. Exemple: Una és la webcam, una altra una presentació i la tercera pot ser una finestra web.
- Emissió en temps real (diferents plataformes) o diferit.

---

## Edició de vídeo (Android)

### Kinemaster

---

## Edició de vídeo (PC)

### Shotcut

- Editor complet i fàcil de fer anar. Amb tutorial a la web
- https://shotcut.org/download/


### Vid Cutter

- Molt senzill, només fa talls. Peta de tant en tant.

https://github.com/ozmartian/vidcutter

### MP4 Box

- Edició de vídeo des de consola - https://github.com/gpac/gpac/wiki/MP4Box

---

## Conversors de formats

https://www.atareao.es/podcast/convertir-formatos-multimedia-en-linux/

### Handbrake

Genial conversor entre formats amb entorn gràfic - https://handbrake.fr/features.php
