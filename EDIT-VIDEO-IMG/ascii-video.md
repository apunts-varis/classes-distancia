

Record and share your terminal sessions, the simple way.
Forget screen recording apps and blurry video. Enjoy a lightweight, purely text-based approach to terminal recording. 

https://asciinema.org
