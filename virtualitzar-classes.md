# Virtualitzar les classes

## COM VIRTUALITZAR UNA SESSIÓ PRESENCIAL

1. Defineix els objectius d'aprenentage per l'alumne:
  - Què ha de saber?
  - Què ha de conèixer?

2. Selecciona els recursos més adients:
  - Videoconferència
  - Vídeo
  - Lectura
  - Debat
  - Podcast
  - Web
  - Activitat
  - Infografia

Pots crear-ne de propis o bé utilitzar-ne d'existents, sempre respectant els drets d'ús indicats a cada cas.

3. Organitza els recursos per presentar-los seguint una seqüència formativa:

- Quin és el millor moment per proposar a l'alumne un repte o un exercici pràctic ?
- Quan exposaré els continguts?
- Quina serà la durada?

4. Crea o facilita els recursos per incorporar-los a l'aula virtual

## Videoconferencia

- La videoconferencia es muy útil en el mundo de la formación online, para mantener contacto sincrónico con los estudiantes.
- Estas reuniones online pueden conversar en torno a un tema predeterminado e intercambiar, a la vez y de forma muy importante, información en diversos formatos.
- Se trata de aprender a aprovechar esta herramienta sincrónica y a sacar partido de sus posibilidades para ir más allá de una mera clase magistral. La videoconferencia nos permite el desarrollo de interesantes experiencias de comunicación.

### Aplicabilidad en el aula

A continuación te facilitamos un listado de usos educativos de la videoconferencia:
- Se puede preparar una videoconferencia con alguna persona experta para presentar casos de éxito o de buenas prácticas y luego generar un tiempo de debate.
- Para resolver dudas sobre, organización de las sesiones o planificación de acciones a realizar durante el módulo o el curso.
- Para transmitir información (como podríamos hacer en una clase presencial) de forma sincrónica con nuestro grupo o clase.
- Asociada o como complemento a cualquier actividad que se quiera realizar en el aula virtual.
- Para organizar reuniones con el equipo de tutores en caso de tener que preparar sesiones compartidas.

### Recomendaciones
- Antes de empezar comprueba las herramientas que vas a usar, micrófono, cámara, iluminación.
- Puedes usar los minutos previos a la hora establecida para testear estos aspectos más técnicos asociados al uso de la herramienta seleccionada.
- Decide antes de empezar si vas a querer grabar la sesión para un posterior visionado de manera asincrónica.

### Organización y planificación
- Prepárate un guion sobre como estructurarás la sesión.
- Decide los contenidos y establece fechas, hora y sobretodo la duración.
- Prepárate los materiales, herramientas y recursos a utilizar. En el caso que quieras puedes explotar las funcionalidades que permiten estas herramientas (compartir pantalla por ejemplo para mostrar una presentación o cualquier otro recurso).
- Prepárate si quieres grabar la sesión. Avisa a tu público que vas a hacerlo y porqué.
- Intenta durante la ejecución gestionar bien el tiempo según el guion.
- Es recomendable quedar unos minutos antes para hacer pruebas de conexión y asegurar que todo el mundo se conecta de forma correcta.
- Ten en cuenta que para gestionar correctamente según qué volumen de videollamadas y debido al tipo de conexión de los usuarios quizás deberás pedir a tu audiencia realizar la videollamada con la cámara apagada.
- Recuerda: tú debes tener el control del evento.

#### Empezamos…
- Preséntate y presenta qué harás y sobretodo como lo harás, es decir qué reglas de funcionamiento se van a seguir durante el tiempo marcado (micros apagados, turnos y orden para intervenciones, etc.)
- Ten previamente ya preparados todos los archivos que desees compartir (PDF, Word, links) o que quieras que los alumnos vean en tu escritorio compartido.

#### Durante…
- Como presentador/a deberás moderar la videoconferencia para asegurarte que no se solapan las intervenciones, e incluso para apagar los micrófonos de los participantes que no estén interviniendo en ese momento.
- Intenta que tu sesión no se convierta en un monólogo, interacciona.

#### Después…
- Concluye la sesión y enlaza con otras actividades y contenidos a explorar en redes o en el aula virtual.
- Resume el contenido y pide opinión, recuerda de proporcionar tu correo electrónico u otra manera de contacto por si han quedado algunas dudas.
- Despídete.

---

## Herramientas

### Webex

Para utilizar Webex es necesario que contactes con la coordinación de tu curso y te indicarán la manera cómo hacerlo.

Tutoriales:
- Pruebe una reunión en línea de Webex real: https://www.webex.com/es/test-meeting.html
- Webex meetings adoption toolkit: https://ebooks.cisco.com/story/meetingsadoptiontoolkit/page/1

## 8x8

8×8 se trata de un servicio online gratuito y que en solo un par de clics te permitirá generar una sala en donde podrás comunicarte a través de una videollamada con otras personas.

Tutoriales:
- Crea videollamadas en segundos con el servicio de 8x8: https://www.tekcrispy.com/2019/11/16/8x8-videoconferencia/

## Hangout

Mensajería multiplataforma desarrollada por Google Inc. Destaca su sistema de llamadas de videoconferencia. En una videollamada pueden participar hasta 10 personas

Tutoriales:
- Ayuda de Hangouts:
- Tutorial videollamadas con Google Hangouts y Google Meet: https://rosaliarte.com/videollamadas-hangouts-meet-tutorial-rosaliarte/
Videoconferencia | 4

## Skype

Skype es una aplicación que permite telefonía por internet y videollamadas. La aplicación permite también el envío de mensajes, imágenes y archivos en diferentes formatos.

La versión gratuita permite alojar un total de 50 usuarios dentro de un mismo grupo de videollamada.

Tutoriales:
- Aprende a utilizar Skype y todas sus funciones: https://es.digitaltrends.com/tendencias/como-usar-skype/
