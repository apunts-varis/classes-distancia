# NOUS PORTÀTILS XTEC

## Portàtils alumnes

F12 boot menu -> setup

1. Encendre
2. Prèmer ESC
3. Arrenca mode reparació
4. Anar a opcions extra
5. Canviar mode UEFI
6. Reiniciar i posar contrasenya BIOS
7. Activar arrencada USB
8. Canviar ordre d'arrencada
9. Desactivar arrencada segura per poder usar VirtualBox directament (o bé signar els mòduls necessaris, més llarg)

- Aquests portàtils no permeten arrencar en mode legacy, només UEFI.
- Per canviar un disc de legacy a UEFI
  - https://www.redhat.com/sysadmin/bios-uefi
  - https://askubuntu.com/questions/913397/how-to-change-ubuntu-install-from-legacy-to-uefi
  - Install boot repair - https://help.ubuntu.com/community/Boot-Repair

---

**Method 1:**
1. Disable the Compatibility Support Module (CSM; aka "legacy mode" or "BIOS mode" support) in your firmware.
2. Boot the Ubuntu installer in its "try before installing" mode.
3. Double-check that you've booted in EFI mode by looking for a directory called /sys/firmware/efi. If it's present, you've booted in EFI mode and can continue; but if it's absent, you've booted in BIOS mode and should review your firmware settings and how you created the boot medium.
4. Download and run the Boot Repair program, as described here.
5. Tell Boot Repair to repair your installation.
6. With any luck, GRUB will appear and give you the option of booting either Windows or Ubuntu. In some rare cases, the system may boot straight to Ubuntu in EFI mode and you'll need to tweak GRUB to get Windows to appear in the GRUB menu.

**Method 2:**
1. Disable the Compatibility Support Module (CSM; aka "legacy mode" or "BIOS mode" support) in your firmware. You may also need to disable Secure Boot, as described on this page of mine, at least temporarily.
2. Download the USB flash drive or CD-R version of my rEFInd boot manager. (Download links for both are on that page.)
3. Prepare the rEFInd boot medium.
4. Reboot into the rEFInd boot medium.
5. Boot to Ubuntu.
6. In Ubuntu, install an EFI-mode boot loader. Two options are easiest:
   1. You can run Boot Repair, as in steps #4 and #5 of Method 1
   1. You can install the Debian package or PPA of rEFInd.
9. GRUB or rEFInd, whichever you specified, should come up and enable you to boot either Windows or Ubuntu.
10. If desired, you can re-enable Secure Boot; however, if you installed rEFInd, you may need to manually configure Shim and install a MOK key, as described in the rEFInd Secure Boot documentation.
---


---

Mirar Telegram profes a partir del 29 d'octubre

## Projecte oficial

https://projectes.xtec.cat/edc/

## Tecles
El clonezilla arrenca.
F1- BIOS
F12- seleccionar unitat d'arrencada

---

## Canviar arrencada

El meu portàtil ja no té Windows, però un company m'ha deixat probar amb el seu. En apagar-lo "malament" prement quatre segons l'interruptor, arrencava preguntant si entrar en mode recuperació, i en escollir opcions avançades de recuperació i escollir obrir una shell, ja pots fer de tot per que la shell sembla que té privilegis de system. Podia copiar fitxers SAM i SYSTEM del registre a un USB, i fer de tot amb els comptes amb comandes "net user".

I si des d'aquesta shell copies el fitxer de "cmd.exe" a alguna utilitat de les que es pot accedir des de la pantalla de login, tens una shell a la pantalla de login

---

## Carregar partició Bitlocker a Windows

Des del Linux s'ha d'utilitzar el dislocker que és una eina per muntar particions encriptades amb bitlocker. He seguit aquesta guia: https://www.linuxuprising.com/2019/04/how-to-mount-bitlocker-encrypted.html

Cal saber la clau de recuperació del bitlocker, que la podem saber
Si entres aquí: aka.ms/aadrecoverykey

L'objectiu és aquest, poder arribar al SAM. bé, doncs s'ha d'executar
~~~
sudo dislocker <partition> -p<clau_de_recuperació> -- /media/bitlocker
~~~

i després ja es pot muntar la partició i usar el chntpw

He seguit aquesta altra guia: https://www.top-password.com/knowledge/reset-windows-10-password-with-ubuntu.html

---

Lo de la shell, per exemple pots fer:

~~~
copy c:\windows\system32\osk.exe c:\windows\system32\osk.exe.bak
copy c:\windows\system32\cmd.exe c:\windows\system32\osk.exe
~~~

i llavors en la pantalla de login escollir que vols "on screen reader" i t'obre la shell

O també pots fer:
~~~
copy c:\windows\system32\utilman.exe c:\windows\system32\utilman.exe.bak
copy c:\windows\system32\cmd.exe c:\windows\system32\utilman.exe
~~~

---

Des d'un Ubuntu jo he pogut habilitar l'usuari Administrador i blanquejar-li el password.

Tot i així no permet iniciar sessió sense contrasenya, però llavors des del teu compte de edu.gencat.cat pots anar a crear un nou usuari  i quan et surt la finestra que demana les credencials d'un administrador, allà poses localhost\Administrador i sense contrasenya. I ja teniu un usuari administrador local 😎

---

## Drivers

Ei, ja em, funciona l'HDMI al portàtil del d'Educació: eren els controladors. https://download.lenovo.com/consumer/mobiles/vnvi050f40xea0.exe

Lector d'emprenta digital - He posat aquest driver:
http://download.windowsupdate.com/d/msdownload/update/driver/drvs/2021/01/278f8892-d3e9-43fc-84e7-81bb58ea8551_26613236f541cabfacbad0667aa3b80e0368186c.cab

### Wifi

Hola,


Ja ho he resolt. Hi ha alguns AP (i de vegades controladors de WiFi no compatibles amb una nova funcionalitat de seguretat que incorpora Linux que són els canvis freqüents d'adreces MAC aleatòries en la connexió).


La solució:

Editem fitxer de configuració de NetworkMAnager:

sudo vi /etc/NetworkManager/NetworkManager.conf

per incloure-hi les línies:


[device]
wifi.scan-rand-mac-address=no

Reiniciem el servei:

sudo systemctl restart network-manager.service


Un cop fet això, la connexió eduroam ja funciona (en el cas dels AP que tenen en aquest centre!).


Font: https://askubuntu.com/questions/910185/rosewill-rnx-n600ube-connectivity-issue-on-ubuntu-17-04

---

## Alumnes

crec que no en fa de res d'aquest email (l'usuari sempre serà codi RALC @edu.gencat.cat o bé alias.codicentre@edu.gencat.cat, però en cap cas serà el correu GSuite de l'alumne - només si és un chromebook serà el d'inici de sessió)

---

## Aplicatiu IDI

Bon dia a tots, algú està utilitzant l'aplicatiu IDI del portal de centres? Estic mirant d'importar àlies dels alumnes emprant la plantilla que et fan  descarregar però em dóna un error.

Abans de carregar cap fitxer, cal clicar a “Descarregar la plantilla” amb el botó vermell de la part superior dreta. Un cop s’ha descarregat la plantilla, cal obrir-la i indicar-hi l’identificador RALC, l’àlies i, si escau, l’usuari de G Suite de cada alumne/a que es vulgui actualitzar.Cal desar la plantilla, sense canviar-ne el format CSV.En cas que hàgiu editat el document amb LibreOffice Calc, heu d'escollir el delimitador "punt i coma" en el moment de desar a format CSV:

https://projectes.xtec.cat/edc/wp-content/uploads/usu2268/2020/12/PEDC_IDI.pdf

## Control remot dels PORTÀTILS

Els ordinadors del Departament venen amb Microsoft Intune, que tal com arrenques el Windows pot forçar tot tipus de canvi al sistema:

https://docs.microsoft.com/en-us/mem/intune/fundamentals/what-is-intune


## Reparar bitlocker

Em va caldre la clau de bitlocker. La clau de bitlocker està a l'adreça web que surt en alguns pantallasos blaus de l'arrencada:.  Https://aka.ms/aadrecoverykey/ amb les mateixes credencials que per entrar al portàtil. Mateix usuari i mateixa contrasenya
Has de picar en el dispositiu
I obtens el codi
Amb ell tot usuari es pot fer ell sol admin
Ara hi ha que arrancar desde l'opcio f8
Anar a recuperar el sistema
obrir un cmd,
ficar la clau bitlocker
I activar el usuari admin del equip que esta desactivat per defecte
Ficar un password i reiniciar

## Muntar bitlocker a linux
https://www.linuxuprising.com/2019/04/how-to-mount-bitlocker-encrypted.html
L'objectiu és aquest, poder arribar al SAM

bé, doncs s'ha d'executar
sudo dislocker <partition> -p<clau_de_recuperació> -- /media/bitlocker

i després ja es pot muntar la partició i usar el chntpw .He seguit aquesta altra guia: https://www.top-password.com/knowledge/reset-windows-10-password-with-ubuntu.html

Lo de la shell, per exemple pots fer:
copy c:\windows\system32\osk.exe c:\windows\system32\osk.exe.bak
copy c:\windows\system32\cmd.exe c:\windows\system32\osk.exe
i llavors en la pantalla de login escollir que vols "on screen reader" i t'obre la shell

O també pots fer:
copy c:\windows\system32\utilman.exe c:\windows\system32\utilman.exe.bak
copy c:\windows\system32\cmd.exe c:\windows\system32\utilman.exe
i llavors en la pantalla de login escollir la icona del mig de les tres icones de baix a la dreta i t'obre la shell

---

A mi el menú de recuperació de restabliment em surt lleugerament diferent al que mostres i demana contrasenya bitlocker per les altres opcions d'inici. Vaja que no puc entrar també com usuari local administrador. Un ordinador inútil a hores d'ara. Alguna idea de com aconseguir-ho amb aquest mètode?

li has tret la xarxa al pc? En principi el mètode encara funciona pel que m'ha anat comentant la gent, per desgràcia com anava fent proves sense documentar, no tinc un manual 😔

[In reply to JAN JAN]
Sí. Sense xarxa

Seleccionant 'quitar todo', posant la contrasenya del bitlocker (s'ha de mirar al perfil de l'usuari a Microsoft) i restaurant sense connexió a Internet dóna l'opció de generar un usuari administrador local.

---

## Portàtils petits alumnes i refind

Crec que he trobat una forma d'arrencar per USB en els portàtils d'ensenyament sense necessitar la password de la BIOS. Si que cal instal·lar un programa com administrador, cosa que en principi pot fer el coordinador de informàtica.Demà faré més proves amb aquestos equips, però per si a algú li interessa he creat un repositori amb una breu documentació del que cal fer.Es tracta d'instal·lar un boot manager anomenat rEFInd que permet arrencar fins i tot discos amb MBR i BIOS.El repositori és: https://github.com/jaumeramos/rEFIndSi alguns proveu ja direu com us ha anat. També podeu provar amb VirtualBox i funciona, és una bona forma de veure el funcionament de UEFI.

JAN JAN:Bona nit! Als portàtils si els doneu uns quants 5 segons i accediu a la interfície de recuperació, podeu obtenir un sistema local i admin
Margacst2:Amb el compte xtec pots donar-te d'alta a Office 365 live i pots utilitzar el Team. Jo amb el compte xtec puc, amb el compte edu.gencat.cat em surt el mateix missatge (potser és perquè encara no tenim els portàtils)
JAN JAN:M'explico millor , s'ha de provocar un fallo de sistema de forma que Windows cregui que no es viable iniciar el sistema, d'aquesta forma llença una interfície on podeu restaurar el sistema borrant configuracións i arxius i deixant únicament el sistema pelat. Fas la instalació seguint els passos i únicament és va embogir al idioma i vaig haver de posar espanyol però per un admin lo que sigue . Un cop finalitzat hauries de ser admin en local sense tenir ni bitlocker ni res de res.

Lógicament, no he obert ni posat usb ni res tot del propi sistema amb la qual cosa...

victor ramos:Per cert, he trobat una manera més "adient" d'entrar al recovery. Al costat esquerre, hi ha un foradet després de la connexió per auriculars. Si amb l'ordinador apagat, apreteu amb un clip durant uns segons, entra al menú de selecció de Lenovo, i una de les opcions es system recovery
