## SoftMaker

- https://www.softmaker.com/es/educacion

- Softmaker Office - Deje que sus documentos hablen por sí mismos: gracias al uso intuitivo, la enorme variedad de funciones y el sobresaliente rendimiento de SoftMaker Office podrá crear documentos, hojas de cálculo y presentaciones de primera clase. -


## Editar PDF

- Flexi PDF: Editor PDF per a windows - https://www.softmaker.com/es/flexipdf

- Pujar a Google Drive i descarregar com a document d'open office.

- Omplir pdf auto - BulkPDF is a free and easy to use freeware software (Open Source), which allows to automatically populate an existing PDF form with different values. Only a spreadsheet (Microsoft Excel 2007/2010/2013, LibreOffice or OpenOffice Calc) with the desired values is required - https://bulkpdf.de/
