# Eines per a exàmens online i enquestes

## Tricider

- Per fer una pregunta i recollir idees. Es pot posar pros i contres de les solucions - https://www.tricider.com/

## Google forms

- Google Forms en opció test i afegint plugin de límit de temps, o bé limitant l'accés al formulari un temps i després treure compartició ??

- Exàmens amb google forms: https://www.youtube.com/watch?time_continue=2&v=dzsXKwyCNxs&feature=emb_logo

- Tutorials Google Forms: https://www.youtube.com/playlist?list=PLE7AmnLpUdMdPLt8Pl5ESNs_camJg8Ovv

---

## exam.net
- With Exam.net, teachers can carry out high-stakes exams on the students’ own digital devices. Exam.net is widely used by Swedish schools to carry out more than one million exams each semester. - https://exam.net/en/skolledning

---

## Moodle
- Moodle tests, combinat amb temps límit.

---
## Revisar plagis

- Moss Stanford - Detecta plagi de codi entre fitxers - https://theory.stanford.edu/~aiken/moss/ 
---

## Classroom

- Classroom, posar examen i requerir lliurament a mà posant un temps límit.
