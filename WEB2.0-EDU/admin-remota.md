# Eines d'administració remota d'equips

## Administració gràfica

- https://www.teamviewer.com
- https://www.join.me/
- http://www.ammyy.com/es/
- http://www.tightvnc.com/
- http://showmypc.com/
- https://play.google.com/store/apps/details?id=com.microsoft.rdc.android&hl=es
- https://chrome.google.com/webstore/detail/chrome-remote-desktop/gbchcmhmhahfdphkhkmpfmihenigjmpp?hl=es
- http://www.mikogo.es/  (presentacions en grup)

---

## Administració de consola

### Teleconsole

- https://www.teleconsole.com/

- Teleconsole is a free service to share your terminal session with people you trust. Your friends can join via a command line via SSH or via their browser over HTTPS. Use this to ask for help or to connect to your own devices sitting behind NAT.

- You can also forward local TCP ports to your friends. Use this feature to allow them access the web applications running on your localhost when you are behind NAT.

~~~
curl https://www.teleconsole.com/get.sh | sh

teleconsole
Requesting a disposable SSH proxy for you...
Checking status of the SSH tunnel...

Your Teleconsole ID: 4bc2b5138360d343379b9043083c48eb7084c3b8
WebUI for this session: https://teleconsole.com/s/4bc2b5138360d343379b9043083c48eb7084c3b8
~~~

Client:
~~~
$ teleconsole join 4bc2b5138360d343379b9043083c48eb7084c3b8
~~~
- Ara tots dosfan servir la mateixa consola del "servidor".

#### Sessions segures amb Teleconsole

- Ho fa amb l'APi d'autenticació de Github. per convidar l'usuari kontsevoy
~~~
$ teleconsole -i kontsevoy
~~~
- També convidant al propietari d'una clau pública
~~~
$ teleconsole -i user_key.pub
~~~
- Per acabar la sessió, cal fer exit.

#### Port forwarding amb Teleconsole

~~~
$ teleconsole -f localhost:5000
// al client
$ teleconsole join 4bc2b5138360d343379b9043083c48eb7084c3b8
ATTENTION: elliot has invited you to access port 5000 on their machine via localhost:9000
~~~
- Es pot obrir el túnel des del costat del Client (en realitat hi ha ssh)
~~~
$ teleconsole -L 9000:localhost:5000 join 4bc2b5138360d343379b9043083c48eb7084c3b8
~~~
- O podem fer forward de ports des del client. Per exemple, fer que el remot visiti http://example.com des de l'ordinador client:
~~~
$ teleconsole -f 9000:example.com:80 join 4bc2b5138360d343379b9043083c48eb7084c3b8
~~~
