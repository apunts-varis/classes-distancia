# Eines d'edició d'imatge i collage de fotos

## Fotowall (el millor per mi)

- Fotowall is an opensource creative tool that lets you play with your pixels as you've always wanted! Make the perfect arrangement with your photos, add text, live video from your webcam and the best internet pictures.

- https://github.com/enricoros/fotowall/tree/master
- https://www.enricoros.com/opensource/fotowall/download/binaries/

## Shape collage

- Collages automàtics amb formes: http://www.shapecollage.com/

## Photo Collage

1. PhotoCollage
2. CI tests status
3. Graphical tool to make photo collage posters
4. PhotoCollage allows you to create photo collage posters. It assembles the input photographs it is given to generate a big poster. Photos are automatically arranged to fill the whole poster, then you can change the final layout, dimensions, border or swap photos in the generated grid. Eventually the final poster image can be saved in any size.
5. The algorithm generates random layouts that place photos while taking advantage of all free space. It tries to fill all space while keeping each photo as large as possible.
6. PhotoCollage does more or less the same as many commercial websites do, but for free and with open-source code.

- https://github.com/adrienverge/PhotoCollage

## Mountain Tapir

1. Arrange images into a collage, and save the result as a new image.
2. Graphical interface gives a preview of the finished collage.
3. Multiplatform (in theory).

- https://github.com/tttppp/mountain_tapir

---
## Captura d'imatges a Ubuntu
- https://www.atareao.es/ubuntu/realizar-una-captura-de-pantalla-en-ubuntu/
---
## Edició d'imatges

- Krita es un programa profesional de pintura digital, gratuito y hecho con código libre, ha sido creado por artistas mismos que desean hacer éstas herramientas accesibles para todos. Arte conceptual, Pinturas de textura y mate
Ilustraciones e historietas. Alternativa a photoshop - https://krita.org/es/
