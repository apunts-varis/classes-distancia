## Contingut interactiu

- H5P makes it easy to create interactive content by providing a range of content types for various needs. Preview and explore these content types below.

- You can create interactive content by adding the H5P plugin to your WordPress, Moodle or Drupal site, or integrate it via LTI with Canvas, Brightspace, Blackboard and many other VLEs that supports LTI integration.

https://h5p.org/content-types-and-applications
