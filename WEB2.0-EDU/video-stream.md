# Vídeo, streaming, descarregar vídeo

## ED-Puzzle

- Aula virtual amb vídeos. Permet controlar quins vídeos es visualitzen i el progrès.
- permet agafar vídeos existents a internet i retallar-los, afegir pauses i ficar tests en mig (atura vídeo i poses preguntes).

---

## TV online

- TDT iptv - http://photocall.tv/

---

## Descarregar Youtube (youtube-dl)

- Youtube-dl - youtube-dl is a command-line program to download videos from YouTube.com and a lot more sites (almost all) (http://ytdl-org.github.io/youtube-dl/supportedsites.html) - https://youtube-dl.org/

---

## Descarregar vídeos de Webinars (JW Player Videos, GotoWebinar)

- Obrir el vídeo del Webinar en Firefox
- Botó dret a la pàgina, picar "informació de la pàgina"
- Ara botó "Multimèdia" i escollir el vídeo.
- Picar botó "anomena i desa"
