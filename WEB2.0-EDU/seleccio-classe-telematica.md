# Classes online

## Tauleta gràfica low-cost (tablet Android com a tauleta)

1. Baixar servidor pel PC des de http://tinyurl.com/nr8yanz:
2. Executar WifiTabletServer.jar
~~~
java -jar WifiTabletServer.jar
~~~
3. Instal·lar client Android (https://play.google.com/store/apps/details?id=com.wifitablet&hl=en&gl=US)

---

## Programes de suport

- Gromit-MPX : el meu favorit, senzill, efectiu, et permet escriure a l'escriptori. Indicacions més avall.
- Vokoscreen : per enregistrar l'escriptori, té una funció per activar la càmera i poder aparèixer sobre el dibuix, molt còmode.
- Xournal++ : programari de pissarra/llibreta d'apunts. Està a la botiga de snap
- Openboard : programari de pissarra, costa una mica d'instal·lar. Proveu amb aquesta release o bé amb el gestor de paquets flatpak aquí.
