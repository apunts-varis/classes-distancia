# Presentacions

## Recull de programes

- https://www.evatarin.net/alternativas-powerpoint-presentaciones/

- Canva - preparar imágenes muy chulas, también sirve para hacer presentaciones con mucha calidad gráfica. Y sigue el principio de Canva, tú no sabes casi de diseño y puedes crear presentaciones muy visuales. Tienes cientos de plantillas para crear presentaciones visuales impactantes. Además, puedes combinarla con su biblioteca de imágenes y sus tipografías.
- Emaze -
- Genial.ly
- Google Presentations o Google slides
- Prezi
- PowToon
- Venngage
